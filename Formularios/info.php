<?php
session_start();
$_SESSION["ultimo_acceso"]= date("Y-n-j H:i:s");
function agregarCol(){
    for($i=1 ; $i <= count($_SESSION['Alumno']); $i++){
        echo '<tr><th>' . $_SESSION['Alumno'][$i]['n_cuenta'] . '</th><th>' . $_SESSION['Alumno'][$i]['nom'] . " " . $_SESSION['Alumno'][$i]['papellido'] . " " . $_SESSION['Alumno'][$i]['sapellido'] . '</th><th>' . $_SESSION['Alumno'][$i]['fecha'] . '</th></tr>';
    }
}
?>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Información</title>
        <link rel="stylesheet" href="styles2.css">
    </head>
    <body>
        <div class="container">              

            <?php
            $cuenta = $_SESSION['cuenta'];
            if( !(isset( $_SESSION['Alumno']) ) ){
                header("Location: login.php?error=El usuario o la contraseña son inválidos");
            }
            ?>

            <!-- Para activar página actual -->
            <?php
            function active($currect_page){
                $url_array = explode('/', $_SERVER['REQUEST_URI']);
                $url = end($url_array);
                if($currect_page == $url){
                echo 'active';
                }
            }
            ?>
        
            <nav>
                <ul class="menu">
                    <li><a class="<?php active('info.php');?>" href="info.php">Home</a></li>
                    <li><a class="<?php active('formulario.php');?>" href="formulario.php">Registrar Alumnos</a></li>
                    <li class="salir" style="margin-left:auto;"><a href="logout.php">Cerrar Sesión</a></li>             
                </ul>            
            </nav>

            <main class="contenido">
                <h1>Usuario Autenticado</h1>
                <table>
                    <tr>
                        <th><img src="https://i.pravatar.cc/128" alt="Foto de perfil" style="float:left;width:60px;border-radius:150px;margin-right:20px;"> 
                            <p></p>
                            <?php echo $_SESSION["Alumno"][$cuenta]['nom'] . ' ' . $_SESSION["Alumno"][$cuenta]['papellido'] . ' ' . $_SESSION["Alumno"][$cuenta]['sapellido']; ?>
                        </th>                      
                    </tr>
                    <tr>
                        <th>
                            <h2>Información</h2>
                            <p>Número de cuenta: <?php echo $cuenta ?></p>
                            <p>Fecha de Nacimiento: <?php echo $_SESSION['Alumno'][$cuenta]['fecha'] ?></p>
                        </th>

                    </tr>
                </table>
                <br>
                <h1>Datos guardados:</h1>

                <table>
                    <tr>
                        <th style="font-weight: bold;">#</th>
                        <th style="font-weight: bold;">Nombre</th>
                        <th style="font-weight: bold;">Fecha de Nacimiento</th>
                    </tr>
                    <?php agregarCol();?>
                </table>
               
            </main>

        </div>

    </body>
</html>
