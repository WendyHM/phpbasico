<?php
session_start();
$hora_actual = date("Y-n-j H:i:s");
$tiempo_transcurrido = (strtotime($hora_actual)-strtotime($_SESSION["ultimo_acceso"]));
if($tiempo_transcurrido >= 36000){
    session_destroy();
}
header("Location: login.php");
?>