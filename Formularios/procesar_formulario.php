<?php

$num_cta = $contrasena = $nombre = $primer_apellido = $segundo_apellido = $genero = $fec_nac = "";

if(!empty($_POST["date"])){
    $fec_nac = $_POST["date"];
    $fec_nac = strftime("%d/%m/%Y", strtotime($fec_nac));
}

if($_SERVER["REQUEST_METHOD"] == "POST"){

    if(!empty($_Post["cuenta"])){
        $num_cta = test_input($_POST["cuenta"]);
        if(!preg_match("/[0-9]+/",$num_cta)){
            $num_ctaErr = "Sólo números enteros";
        }
    }

    if(empty($_POST["texto"])){
        $nombreErr = "El nombre es obligatorio";
    }else{
        $nombre = test_input($_POST["texto"]);
        if(!preg_match("/^[a-zA-Z-' ]*$/", $nombre)){
            $nombreErr = "Sólo letras y espacios.";
        }
    }

    if(empty($_POST["lname"])){
        $primer_apellidoErr = "El primer apellido es obligatorio."; 
    }else{
        $primer_apellido = test_input($_POST["lname"]);
        if(!preg_match("/^[a-zA-Z-' ]*$/", $primer_apellido)){
            $nombreErr = "Sólo letras y espacios.";
        }
    }

    if(!empty($_POST["sname"])){
        $segundo_apellido = test_input($_POST["sname"]);
        if(!preg_match("/^[a-zA-Z-' ]*$/", $segundo_apellido)){
            $segundo_apellidoErr = "Sólo letras y espacios.";
        }
    }

    if(!empty($_POST["gender"])){
        $genero = test_input($_POST["gender"]);
    }

    if(empty($_POST["password"])){
        $contrasenaErr = "La contraseña es obligatoria.";
    }else{
        $contrasena = test_input($_POST["password"]);
    }
}

function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if( !(empty($contrasena) && empty($num_cta)) ){
    $tam_array = count($_SESSION['Alumno']);
    $cuenta_nueva = $tam_array + 1;

    $_SESSION['Alumno'][$cuenta_nueva]= array(
        'n_cuenta' => $cuenta_nueva,
        'contrasena' => $contrasena,
        'nom' => $nombre,
        'papellido' => $primer_apellido,
        'sapellido' => $segundo_apellido,
        'genero' => $genero,
        'fecha' => $fec_nac
    );

    header("Location: info.php");
}

?>