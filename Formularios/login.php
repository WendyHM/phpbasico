<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div class="container">
            <div id="boxlogin">
                <h1 id="login">Login</h1>
                <form action="procesar_login.php" method="POST">
                     
                    <!-- Mensaje de error para validación incorrecta -->
                    <?php 
                    // isset determina si una variable está definida y no es NULL 
                    if(isset($_GET['error'])){
                        echo '<p class="error" style="color: red;">El usuario o contraseña son inválidos.</p>';
                    } ?>

                    <!-- Número de Cuenta -->
                    <label class="form-label" for="cuenta">Número de Cuenta:</label>
                    <br>
                    <input class="form-input" name="cuenta" type="text" id="input-num-cta" 
                        placeholder="Número de cuenta">
                    <br>
                    
                    <!-- Contraseña  -->
                    <label class="form-label" for="password">Contraseña:</label>
                    <br>
                    <input class="form-input" name="password" type="password" id="input-password"
                        placeholder="Contraseña">
                    <br>
                    <input class="btn" type="submit" value="Entrar"/>
                
                </form>
            </div>
        </div>
    </body>
</html>
