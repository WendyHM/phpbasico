<html>
    <head>
        <meta charset="UTF-8">
        <title>Formulario</title>
        <link rel="stylesheet" href="styles2.css">
    </head>
    <body>
        <div class="container">

            <?php
            session_start();
            $_SESSION["ultimo_acceso"]= date("Y-n-j H:i:s");
            if( isset( $_SESSION['Alumno'] ) ){
                //Eliminar al final
            }else{
                header("Location: login.php?error=El usuario o la contraseña son inválidos");
            }
            ?>

            <!-- Para activar página actual -->
            <?php
            function active($currect_page){
                $url_array = explode('/', $_SERVER['REQUEST_URI']);
                $url = end($url_array);
                if($currect_page == $url){
                echo 'active';
                }
            }
            ?>

            <!-- Valida formulario -->
            <?php 
            include("procesar_formulario.php");
            ?>

            <nav>
                <ul class="menu">
                    <li><a class="<?php active('info.php');?>" href="info.php">Home</a></li>
                    <li><a class="<?php active('formulario.php');?>" href="formulario.php">Registrar Alumnos</a></li>
                    <li class="salir" style="margin-left:auto;"><a href="logout.php">Cerrar Sesión</a></li>             
                </ul>            
            </nav>

            <main>

                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="POST">

                    <label class="form-label" for="input-num-cta">Número de cuenta</label>
                    <input name="cuenta" class="form-input" type="text" id="input-num-cta"
                        placeholder="Número de cuenta" >              
                    <br>           
                  
                    <label class="form-label" for="input-name">Nombre</label>
                    <input name="texto" class="form-input" type="text" id="input-name" 
                        placeholder="Nombre">
                    <?php if( isset($nombreErr) ){
                    echo '<span class="error" style="color:red;font-size:15px;"> &nbsp * ' . $nombreErr . '</span>'; 
                    }
                    ?>     
                    <br>                       
                    
                    <label class="form-label" for="input-lname">Primer Apellido</label>
                    <input name="lname" class="form-input" type="text" id="input-lname"
                        placeholder="Primer Apellido">
                    <?php if( isset($primer_apellidoErr) ){
                    echo '<span class="error" style="color:red;font-size:15px;"> &nbsp * ' . $primer_apellidoErr . '</span>'; 
                    }
                    ?>    
                    <br>                
                    
                    <label class="form-label" for="input-sname">Segundo Apellido</label>
                    <input name="sname" class="form-input" type="text" id="input-sname"
                        placeholder="Segundo Apellido">
                    <br>               
              
                    <label class="form-label">Género</label>
                    <div class="box">
                    <label class="form-radio">
                        <input class="icon" type="radio" name="gender" value="M">
                        <i class="form-icon"></i>Mujer
                    </label>

                    <label class="form-radio">
                        <input class="icon" type="radio" name="gender" value="H">
                        <i class="form-icon"></i>Hombre
                    </label> 

                    <label class="form-radio">
                        <input class="icon" type="radio" name="gender" value="O">
                        <i class="form-icon"></i>Otro
                    </label><br>  
                    </div>                 
                    
                    <label class="form-label" for="input-date">Fecha de Nacimiento</label>
                    <input name="date" class="form-input" type="date" id="input-date"
                        placeholder="fecha"><br>                
                
                    <label class="form-label" for="input-password">Contraseña</label>
                    <input name="password" class="form-input" type="password" id="input-password"
                        placeholder="Contraseña">
                    <?php if( isset($contrasenaErr) ){
                    echo '<span class="error" style="color:red;font-size:15px;"> &nbsp * ' . $contrasenaErr . '</span>'; 
                    }
                    ?>   
                    <br>
                    
                    <input type="submit" class="btn" value="Registrar"/>               
                </form>            

            </main>
        </div>
    </body>
</html>
