# Ejercicios del curso "Lenguaje de programación PHP".

En este repositorio se encuentran las actividades relacionadas a la primera parte del curso de PHP.

## Autor 

* Hernández Méndez Wendy

### Contacto

wendy.hm005@cianecias.unam.mx

## Instructores 

* Barajas González Daniel
* Cuéllar Martínez Hugo
* Fonseca Márquez Karla



